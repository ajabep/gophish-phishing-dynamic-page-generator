# GoPhish Phishing Dynamic Page Generator

The goal of this project is to provide a small and simple framework to generate dynamic page for phishing aims. The generated resources are to be used with GoPhish.


## Legal

I decline any responsibility of the usage of this tool. I provide it for phishing exercises, not for phishing attacks.

I know that this tool may be used for real world attacks, but, be sure of that, attackers already have this kind of tools, and are already doing things worse than what this tool allows.

So, to decrease, at my scale, the risk of this country, I just remind, at the end of this document, the state of the art to counter (spear-)phishing attacks.

Moreover, to reduce the risk to see this tool used in a real world, wise that a GoPhish server is not that difficult to identify by using a proxy server. Finally, for the same reasons, this tools is not enough easy to use by script-kiddies, because it requires basic technical skills.


## How to use

To use commands, you will need to install dependencies, with `poetry install`.

Then, to define a phishing page, you will need to:

1. Capture the initial HTML of the targeted page in `src/index.html`. FYI, it's Jinja template. Variables and functions given to any Jinja templates are described bellow.
2. Add `{{ headers }}` in `<head>` tag and `{{ endpage }}` at the end of the `<body>` tag. These will replace by, respectively, styles and a mix between form and scripts. **These Jinja variables are only available in your `index.html` file.**
3. Capture all CSS stuff in `src/style.css`. It will be used be a Jinja template, actually.
4. In `src/index.html` and `src/style.css` replace any static resources to clone without changing anything by `{{ clone("http://ABSOLUTE.URL") }}`.
5. Add static assets in `src/static/`. You will have the possibility to download them automatically. Here are only the one you want to edit.
6. Describe steps into `src/phishing_steps.toml`. Some properties are also Jinja templates. For more info, read details below.
7. Add custom JS into `src/script.js` if needed.

Once everything is done, start the test server using `poetry run devserve`. Once everything is done, execute `poetry run generate` and put the generated resources as:

1. The `index.html` as a "Landing Page".
2. The files in the generated `static/` directory in the gophish `static/endpoint/` directory.


### TOML Event Definition (`src/phishing_steps.toml`)

The goal of this file is to define events which will occurred in the page. You have, with all this stuff, to fill the `#{{random_id}}-email` and `#{{random_id}}-password` fields and submit the form `#{{random_id}}-form` when you want have to credentials. This mechanism is used to be compatible with the GoPhish parameters of landing pages ("Capture data" out-out, "Capture password" opt-out, etc.)

The TOML file schema is the following. Some properties are Jinja templates.

#### Table Of Content
- Root
	- Property `root > events`
		- `root > events > step_onload`
			- Property `root > events > step_onload > execjs`
			- Property `root > events > step_onload > replacehtml`
				- Property `root > events > step_onload > replacehtml > targetID`
				- Property `root > events > step_onload > replacehtml > targetSelector`
				- Property `root > events > step_onload > replacehtml > replace`
		- `root > events > step`
			- Property `root > events > step > requires`
				- root > events > step > requires > requires items
			- Property `root > events > step > event`
				- Property `root > events > step > event > id`
				- Property `root > events > step > event > selector`
				- Property `root > events > step > event > type`
			- Property `root > events > step > execif`
			- Property `root > events > step > exectimeout`
			- Property `root > events > step > execjs`
			- Property `root > events > step > replacehtml`


#### Root

|                           |             |
|---------------------------|-------------|
| **Type**                  | `object`    |
| **Required**              | No          |
| **Additional properties** | Not allowed |

| Property | Pattern | Type  | Title/Description                     |
|----------|---------|-------|---------------------------------------|
| `events` | No      | array | Array of events executed on the page. |

#### Property `root > events`

|              |         |
|--------------|---------|
| **Type**     | `array` |
| **Required** | Yes     |

|                      | Array restrictions |
|----------------------|--------------------|
| **Min items**        | 0                  |
| **Max items**        | N/A                |
| **Items unicity**    | False              |
| **Additional items** | False              |
| **Tuple validation** | See below          |

| Each item of this array must be | Description                                              |
|---------------------------------|----------------------------------------------------------|
| `step_onload` (first element)   | Required. First event, executed when the page is loaded. |
| `step` (rest of the array)      | Any other events executed in the page.                   |


#### `root > events > step_onload`

|                           |                       |
|---------------------------|-----------------------|
| **Type**                  | `object`              |
| **Required**              | No                    |
| **Additional properties** | Not allowed           |
| **Defined in**            | `#/$defs/step_onload` |

| Property      | Pattern | Type   | Deprecated | Definition               | Title/Description                                                                                                     |
|---------------|---------|--------|------------|--------------------------|-----------------------------------------------------------------------------------------------------------------------|
| `execjs`      | No      | string | No         | In `#/$defs/execjs`      | Raw JavaScript code executed when the event occurred. It's a Jinja template.                                          |
| `replacehtml` | No      | object | No         | In `#/$defs/replacehtml` | Describe which piece of the DOM to replace by with HTML code. This can be useful to change a big portion of the page. |`


#### Property `root > events > step_onload > execjs`

|                 |                                                                              |
|-----------------|------------------------------------------------------------------------------|
| **Type**        | `string`                                                                     |
| **Required**    | No                                                                           |
| **Defined in**  | `#/$defs/execjs`                                                             |
| **Description** | Raw JavaScript code executed when the event occurred. It's a Jinja template. |

To manipulate the current event, you can use the `event` variable, as:

```javascript
// Will cancel the current event
event.preventDefault();
```


#### Property `root > events > step_onload > replacehtml`

|                           |                                                                                                                       |
|---------------------------|-----------------------------------------------------------------------------------------------------------------------|
| **Type**                  | `object`                                                                                                              |
| **Required**              | No                                                                                                                    |
| **Additional properties** | Not allowed                                                                                                           |
| **Defined in**            | `#/$defs/replacehtml`                                                                                                 |
| **Description**           | Describe which piece of the DOM to replace by with HTML code. This can be useful to change a big portion of the page. |

| Property         | Pattern | Type   | Deprecated | Definition | Title/Description                                                                      |
|------------------|---------|--------|------------|------------|----------------------------------------------------------------------------------------|
| `targetID`       | No      | string | No         | -          | ID property of the targeted element. The `replace` HTML code will be inserted inside.  |
| `targetSelector` | No      | string | No         | -          | Selector of the HTML element to bind. The `replace` HTML code will be inserted inside. |
| `replace`        | No      | string | No         | -          | Raw HTML code to insert inside the targeted element. It's a Jinja template.            |

`targetID` and `targetSelector` property cannot be used in the same time. However, you have to indicate one.

#### Property `root > events > step_onload > replacehtml > targetID`

|                 |                                                                                       |
|-----------------|---------------------------------------------------------------------------------------|
| **Type**        | `string`                                                                              |
| **Required**    | Yes                                                                                   |
| **Description** | ID property of the targeted element. The `replace` HTML code will be inserted inside. |

#### Property `root > events > step_onload > replacehtml > targetSelector`

|                 |                                                                                        |
|-----------------|----------------------------------------------------------------------------------------|
| **Type**        | `string`                                                                               |
| **Required**    | Yes                                                                                    |
| **Description** | Selector of the HTML element to bind. The `replace` HTML code will be inserted inside. |

#### Property `root > events > step_onload > replacehtml > replace`

|                 |                                                                             |
|-----------------|-----------------------------------------------------------------------------|
| **Type**        | `string`                                                                    |
| **Required**    | Yes                                                                         |
| **Description** | Raw HTML code to insert inside the targeted element. It's a Jinja template. |


#### `root > events > step`

`root > events > step` extends `root > events > step_onload`.

|                           |                |
|---------------------------|----------------|
| **Type**                  | `object`       |
| **Required**              | No             |
| **Additional properties** | Not allowed    |
| **Defined in**            | `#/$defs/step` |

| Property      | Pattern | Type            | Deprecated | Definition                                          | Title/Description                                                                                                                                                 |
|---------------|---------|-----------------|------------|-----------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `requires`    | No      | array of number | No         | -                                                   | After which event this event is able to occurred. This is used to know when we have to bind the element.                                                          |
| `event`       | No      | object          | No         | In `#/$defs/event`                                  | Describe on which JS event this is executed.                                                                                                                      |
| `execif`      | No      | string          | No         | In `#/$defs/execif`                                 | Used to execute this event only on certain condition. Raw JavaScript code, **RETURNING** a boolean. It will be encapsulated in a function. It's a Jinja template. |
| `exectimeout` | No      | integer         | No         | In `#/$defs/exectimeout`                            | Execute this event after N-ms.                                                                                                                                    |
| `execjs`      | No      | string          | No         | Same as `root > events > step_onload > execjs`      | Cf. `root > events > step_onload > execjs`.                                                                                                                       |
| `replacehtml` | No      | object          | No         | Same as `root > events > step_onload > replacehtml` | Cf. `root > events > step_onload > replacehtml`.                                                                                                                  |


#### Property `root > events > step > requires`

|                 |                                                                                                          |
|-----------------|----------------------------------------------------------------------------------------------------------|
| **Type**        | `array of number`                                                                                        |
| **Required**    | Yes                                                                                                      |
| **Description** | After which event this event is able to occurred. This is used to know when we have to bind the element. |

|                      | Array restrictions |
|----------------------|--------------------|
| **Min items**        | 1                  |
| **Max items**        | N/A                |
| **Items unicity**    | True               |
| **Additional items** | False              |


#### `root > events > step > requires > requires items`

|                 |          |
|-----------------|----------|
| **Type**        | `number` |
| **Required**    | No       |


#### Property `root > events > step > event`

|                           |                                              |
|---------------------------|----------------------------------------------|
| **Type**                  | `object`                                     |
| **Required**              | No                                           |
| **Additional properties** | Not allowed                                  |
| **Defined in**            | `#/$defs/event`                              |
| **Description**           | Describe on which JS event this is executed. |

| Property   | Pattern | Type             | Deprecated | Definition | Title/Description                                                   |
|------------|---------|------------------|------------|------------|---------------------------------------------------------------------|
| `id`       | No      | string           | No         | -          | `id` property of the HTML element to bind. It's a Jinja template.   |
| `selector` | No      | string           | No         | -          | Selector of the HTML element to bind. It's a Jinja template.        |
| `type`     | No      | enum (of string) | No         | -          | Event name of which one to bind (e.g.: `click`, `mouseenter`, etc.) |

`id` and `selector` property cannot be used in the same time. However, you have to indicate one.


#### Property `root > events > step > event > id`

|                 |                                                                   |
|-----------------|-------------------------------------------------------------------|
| **Type**        | `string`                                                          |
| **Required**    | No                                                                |
| **Description** | `id` property of the HTML element to bind. It's a Jinja template. |


#### Property `root > events > step > event > selector`

|                 |                                                              |
|-----------------|--------------------------------------------------------------|
| **Type**        | `string`                                                     |
| **Required**    | No                                                           |
| **Description** | Selector of the HTML element to bind. It's a Jinja template. |


#### Property `root > events > step > event > type`

|                 |                                                                     |
|-----------------|---------------------------------------------------------------------|
| **Type**        | `enum (of string)`                                                  |
| **Required**    | Yes                                                                 |
| **Description** | Event name of which one to bind (e.g.: `click`, `mouseenter`, etc.) |


#### Property `root > events > step > execif`

|                 |                                                                                                                                                                   |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Type**        | `string`                                                                                                                                                          |
| **Required**    | No                                                                                                                                                                |
| **Defined in**  | `#/$defs/execif`                                                                                                                                                  |
| **Description** | Used to execute this event only on certain condition. Raw JavaScript code, **RETURNING** a boolean. It will be encapsulated in a function. It's a Jinja template. |


#### Property `root > events > step > exectimeout`

|                 |                                |
|-----------------|--------------------------------|
| **Type**        | `integer`                      |
| **Required**    | No                             |
| **Defined in**  | `#/$defs/exectimeout`          |
| **Description** | Execute this event after N-ms. |

| Restrictions |     |
|--------------|-----|
| **Minimum**  | 0   |


#### Property `root > events > step > execjs`

Cf. `root > events > step_onload > execjs`.


#### Property `root > events > step > replacehtml`

Cf. `root > events > step_onload > replacehtml`.


## Commands

### `devserve`

This command will open a server on `http://127.0.0.1:5000` for development purpose


### `generate`

This command will generate the website into the directory specified with in the `FREEZER_DESTINATION` environment variable, or, as default value, into the `build` one.


## Jinja globals

### Variables

The variables available in any files are the following:

|                 |                                                                                |
|-----------------|--------------------------------------------------------------------------------|
| **`random_id`** | A long hex and random string used to identify the form to will and its fields. |


### Functions

The functions available in any files are the following:

|                                |                                                                                                                                                                           |
|--------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **`clone(url: str) -> str`**   | A function used to clone a remote static resource and put it, as it, in the static directory. The returned string is the good relative path to the right cloned resource. |
| **`static(path: str) -> str`** | A function used to prefix the path with the static directory path.                                                                                                        |


### Filters

No specific Jinja filters are available.


## How to counter (spear-)phishing attacks?

To avoid (spear-)phishing attacks, history showed us these facts:

1. Human factor is not reliable (tiredness, inattention, permanent harassment by the "magic" of computers and by bugs, etc.)
2. 2FA can be a way to counter it, but it is **not necessary** the case. It depends on how it works.
3. Crypto matters! Cryptography works! Maths works! Use it! Over and over and over! Always! For everything! Everywhere!
4. Attackers will always find a way to counter your authentication. (Spear-)phishing reliable 2FA will just decrease drastically of likelihood of it. However, you **CAN NOT** be able to avoid any identity theft. So, privileges separation matters! A lot! And logging also!

Without 2FA, the password can be reused, guessed, and can be abused by many ways. To protect correctly your organization, implements 2FA.

However, 2FA can be transmitted to the attacker which will use it on the real website in real time. So, to be reliable over (spear-)phishing attacks, it has to include the domain, or even the URL, of the page generating the challenge. To do so, use FIDO2/Webauthn or U2F. The rest, as TOTP/HTOP, SMS, Email, QR-code to scan, phone call, push notifications, secret question, a second password to fill, are **NOT** (spear-)phishing-proof.


### If an employee has been breach, is it their fault?

No!

It's OK for employees to have been phished. The only way to prevent (spear-)phishing is much more about technical measures (see above) and not only the human factor (tiredness, inattention). The relevance of phishing campaign is remind to employee that they have to report suspicious emails.


### Are Push-notification secure enough for 2FA?

No. That's how Lapsus ransomware group find way to breach many company (including Nvidia, Uber, etc.). Their *modus operandi* was to harass employee with notification during the night until they accept it. If the employee need to be pushed to click on the right button, or to fill with a number, they will call them saying they are the technical support, and giving data to fill out.


### Is TOTP/HOTP secure enough for 2FA?

No. This can easily be passed over to the right website, and this last will have no clue to know if it's the real user or not.


## TODO

1. Make a tool, with a generator to inject in the real page to facilitate the capture of element.
2. Write tests.
3. Integrate it to Evilginx 2
