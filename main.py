#!/usr/bin/env python3
import hashlib
import json
import os
import sys
import typing
from pathlib import Path
from textwrap import dedent
from urllib.parse import urlparse

import jsonschema as jsonschema
import rcssmin
import requests as requests
import rjsmin
import tomli as tomli
from flask import Flask, render_template_string, Response
from markupsafe import Markup
from werkzeug.exceptions import NotFound
from flask_frozen import Freezer
import minify_html

CURRENT_DIR = Path(os.path.dirname(os.path.abspath(__file__)))
SRC_DIR = CURRENT_DIR / 'src'
STATIC_DIR = SRC_DIR / 'static'

app = Flask(__name__)
app.config["SECRET_KEY"] = os.urandom(16)
app.config["FREEZER_DESTINATION"] = os.environ.get('FREEZER_DESTINATION', CURRENT_DIR / 'build')
freezer = Freezer(app)

RANDOM_ID = os.urandom(16).hex()
CLONE_URLS: typing.Dict[str, str] = {}
CLONE_GENERATED_URLS: typing.List[str] = []

# noinspection SpellCheckingInspection
MIMETYPES = {
    "3g2": "video/3gpp2",
    "3gp": "video/3gpp",
    "7z": "application/x-7z-compressed",
    "aac": "audio/aac",
    "abw": "application/x-abiword",
    "arc": "application/x-freearc",
    "avi": "video/x-msvideo",
    "avif": "image/avif",
    "azw": "application/vnd.amazon.ebook",
    "bin": "application/octet-stream",
    "bmp": "image/bmp",
    "bz": "application/x-bzip",
    "bz2": "application/x-bzip2",
    "cda": "application/x-cdf",
    "csh": "application/x-csh",
    "css": "text/css",
    "csv": "text/csv",
    "doc": "application/msword",
    "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "eot": "application/vnd.ms-fontobject",
    "epub": "application/epub+zip",
    "gif": "image/gif",
    "gz": "application/gzip",
    "htm": "text/html",
    "html": "text/html",
    "ico": "image/x-icon",
    "ics": "text/calendar",
    "jar": "application/java-archive",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "js": "application/javascript",
    "json": "application/json",
    "jsonld": "application/ld+json",
    "mid": "audio/midi",
    "midi": "audio/midi",
    "mjs": "text/javascript",
    "mp3": "audio/mpeg",
    "mp4": "video/mp4",
    "mpeg": "video/mpeg",
    "mpkg": "application/vnd.apple.installer+xml",
    "odp": "application/vnd.oasis.opendocument.presentation",
    "ods": "application/vnd.oasis.opendocument.spreadsheet",
    "odt": "application/vnd.oasis.opendocument.text",
    "oga": "audio/ogg",
    "ogv": "video/ogg",
    "ogx": "application/ogg",
    "opus": "audio/opus",
    "otf": "font/otf",
    "pdf": "application/pdf",
    "php": "application/x-httpd-php",
    "png": "image/png",
    "ppt": "application/vnd.ms-powerpoint",
    "pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "rar": "application/vnd.rar",
    "rtf": "application/rtf",
    "sh": "application/x-sh",
    "svg": "image/svg+xml",
    "tar": "application/x-tar",
    "tif": "image/tiff",
    "tiff": "image/tiff",
    "ts": "video/mp2t",
    "ttf": "font/ttf",
    "txt": "text/plain",
    "vsd": "application/vnd.visio",
    "wav": "audio/wav",
    "weba": "audio/webm",
    "webm": "video/webm",
    "webp": "image/webp",
    "woff": "font/woff",
    "woff2": "font/woff2",
    "xhtml": "application/xhtml+xml",
    "xls": "application/vnd.ms-excel",
    "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "xml": "application/xml",
    "xul": "application/vnd.mozilla.xul+xml",
    "zip": "application/zip",
}


def clone_static_asset(url: str, ext: str = ''):
    global CLONE_URLS
    parsed_url = urlparse(url)
    file = parsed_url.path.split('/')
    extensions = file[-1].split('.')
    if ext != '':
        extensions = ext.split('.')
    filename = hashlib.md5(url.encode('ascii')).hexdigest()
    CLONE_URLS[filename] = url

    if (extensions[-1] == 'js' or extensions[-1] == 'js') and (len(extensions) < 2 or extensions[-2] != 'min'):
        filename += '.min'

    filename += '.' + extensions[-1]
    if len(parsed_url.query) > 0:
        filename += '?' + parsed_url.query
    if len(parsed_url.fragment) > 0:
        filename += '#' + parsed_url.fragment
    filename = '/static/' + filename
    if filename not in CLONE_GENERATED_URLS:
        CLONE_GENERATED_URLS.append(filename)
    return filename


def static_path(filepath: str) -> str:
    return '/static/' + filepath


@app.context_processor
def jinja_inject():
    return dict(
        static=static_path,
        clone=clone_static_asset,
        random_id=RANDOM_ID
    )


def my_make_response(output: typing.Union[typing.AnyStr, os.PathLike, typing.IO],
                     extension: typing.Optional[str] = None, mimetype: typing.Optional[str] = None) -> Response:
    if mimetype is None:
        if extension is None:
            raise AttributeError('Have to give an extension of a mimetype')
        if '.' in extension:
            extension = extension.split('.')[-1]
        mimetype = MIMETYPES.get(extension, '')

    if isinstance(output, typing.IO):
        output = output.read()

    from flask import make_response
    res = make_response(output)
    if mimetype != '':
        res.mimetype = mimetype
    return res


def get_remote_static_resource(url: str) -> bytes:
    r = requests.get(url)
    return r.content


@app.route("/static/<path:path>/<filename>.min.css")
@app.route("/static/<filename>.min.css")
def minification_css(filename: str, path: str = "") -> Response:
    """Minify CSS to optimize the delivery, as any website does because it's a good practice"""
    file = STATIC_DIR / path / f'{filename}.css'
    if file.exists():
        with open(file) as cssfile:
            css = cssfile.read()
        return my_make_response(
            rcssmin.cssmin(
                css
            ),
            'css'
        )

    file = STATIC_DIR / path / f'{filename}.min.css'
    if file.exists():
        return my_make_response(open(file), 'css')

    hash_name = filename.split('.')[0]
    if hash_name in CLONE_URLS:
        url = CLONE_URLS[hash_name]
        return my_make_response(
            rcssmin.cssmin(
                get_remote_static_resource(url)
            ),
            'css'
        )

    raise NotFound()


@app.route("/static/<path:path>/<filename>.min.js")
@app.route("/static/<filename>.min.js")
def minification_js(filename: str, path: str = "") -> Response:
    """Minify JS to optimize the delivery, as any website does because it's a good practice"""
    file = STATIC_DIR / path / f'{filename}.js'
    if file.exists():
        with open(file) as jsfile:
            js = jsfile.read()
        return my_make_response(
            rjsmin.jsmin(
                js
            ),
            'js'
        )

    file = STATIC_DIR / path / f'{filename}.min.js'
    if file.exists():
        return my_make_response(open(file), 'js')

    hash_name = filename.split('.')[0]
    if hash_name in CLONE_URLS:
        url = CLONE_URLS[hash_name]
        return my_make_response(
            rjsmin.jsmin(
                get_remote_static_resource(url)
            ),
            'js'
        )

    raise NotFound()


@app.route("/static/<path:path>/<filename>")
@app.route("/static/<filename>")
def static_serve(filename: str, path: str = "") -> Response:
    file = STATIC_DIR / path / filename
    if file.exists():
        return my_make_response(open(file, 'rb').read(), filename)

    hash_name = filename.split('.')[0]
    if hash_name in CLONE_URLS:
        url = CLONE_URLS[hash_name]
        content = get_remote_static_resource(url)
        return my_make_response(content, filename)

    raise NotFound()


@app.route("/")
def index() -> Response:
    phishing_steps = tomli.load(open(SRC_DIR / 'phishing_steps.toml', 'rb'))
    jsonschema.validate(instance=phishing_steps, schema=json.load(open('schema_phishing_steps.json')))

    # Apply Jinja templating
    def event_apply_jinja(i: int) -> None:

        if phishing_steps['events'][i].get('execjs') is not None:
            phishing_steps['events'][i]['execjs'] = rjsmin.jsmin(
                render_template_string(phishing_steps['events'][i]['execjs'])
            )
        if phishing_steps['events'][i].get('execif') is not None:
            phishing_steps['events'][i]['execif'] = rjsmin.jsmin(
                render_template_string(phishing_steps['events'][i]['execif'])
            )
        if phishing_steps['events'][i].get('replacehtml') is not None:
            phishing_steps['events'][i]['replacehtml']['replace'] = minify_html.minify(
                render_template_string(phishing_steps['events'][i]['replacehtml']['replace']),
                minify_js=True,
                remove_processing_instructions=True
            )

            if phishing_steps['events'][i]['replacehtml'].get('id') is not None:
                phishing_steps['events'][i]['replacehtml']['id'] =\
                    render_template_string(phishing_steps['events'][i]['replacehtml']['id'])
            if phishing_steps['events'][i]['replacehtml'].get('selector') is not None:
                phishing_steps['events'][i]['replacehtml']['selector'] = \
                    render_template_string(phishing_steps['events'][i]['replacehtml']['selector'])

    for k, _ in enumerate(phishing_steps['events']):
        event_apply_jinja(k)

    # Let's build other Jinja tags
    style_code = render_template_string(rcssmin.cssmin(open(SRC_DIR / 'style.css').read()))
    headers = Markup(dedent(
        f"""<style>{style_code}</style>"""
    ))

    endpage = Markup(dedent(
        f"""
        <script>{
        rjsmin.jsmin(
            'var phy=' + json.dumps(phishing_steps) + ';'
            + open(CURRENT_DIR / 'phishing_framework.js').read()
            + open(SRC_DIR / 'script.js').read()
        )
        }</script>
        <form action="" method="POST" style="display:none" id="{RANDOM_ID}-form">
            <input type="email" name="email" id="{RANDOM_ID}-email"/>
            <input type="password" name="password" id="{RANDOM_ID}-password"/>
        </form>
        """
    ))

    return Response(
        render_template_string(
            open(SRC_DIR / 'index.html').read(),
            headers=headers,
            endpage=endpage,
        )
    )


@freezer.register_generator
def clone_url_freezer():
    i = 0
    # Not a for loop: we can grow this while parsing
    while i < len(CLONE_GENERATED_URLS):
        yield CLONE_GENERATED_URLS[i]
        i += 1


@freezer.register_generator
def static_freezing():
    for root, dirs, files in os.walk(STATIC_DIR):
        for filename in files:
            if filename == '.gitkeep':
                continue
            if not root.startswith(str(STATIC_DIR)):
                continue
            yield '/' + root[len(str(STATIC_DIR)):] + '/' + filename
            if (filename.endswith('.js') or filename.endswith('.css')) and '.min' not in filename:
                yield '/' + root[len(str(STATIC_DIR)):] + '/' + '.'.join(
                    filename.split('.')[:-1] + [
                        'min',
                        filename.split('.')[-1]
                    ]
                )


def generate():
    freezer.freeze()


def serve():
    sys.argv = [  # For compatibility with the behavior of Flask
        os.path.basename(os.path.abspath(__file__)),
        'devserve'
    ]
    app.run(debug=True, use_reloader=True)


if __name__ == "__main__":
    """For compatibility with the default behavior of Flask"""
    if sys.argv[1].lower() in ["devserve", "serve"]:
        serve()
    elif sys.argv[1].lower() == "generate":
        generate()
    else:
        raise NotImplementedError(f'Command {sys.argv} not implemented')
