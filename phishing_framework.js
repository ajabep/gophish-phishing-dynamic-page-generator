/*global phy*/
(function(document, _){
	var exec = function(event_index) {
		if (typeof phy['events'][event_index]['execif'] !== 'undefined') {
			eval('var execif=function (){' + phy['events'][event_index]['execif'] + '};');
			/*global execif*/
			if (!execif()) {
				return false;
			}
		}

		if (typeof phy['events'][event_index]['execjs'] !== 'undefined') {
			eval(phy['events'][event_index]['execjs']);
		}
		if (typeof phy['events'][event_index]['replacehtml'] !== 'undefined') {
			var elements = null;
			if (typeof phy['events'][event_index]['replacehtml']['targetSelector'] !== 'undefined') {
				elements = document.querySelectorAll(phy['events'][event_index]['replacehtml']['targetSelector']);
			} else {
				elements = [
					document.getElementById(phy['events'][event_index]['replacehtml']['targetID'])
				];
			}
			for (var i = 0, c = elements.length; i < c; ++i) {
				elements[i].innerHTML = phy['events'][event_index]['replacehtml']['replace'];
			}
		}
	};
	var gen_exec_event = function(event_index) {
		return function(_event){
			var my_exec_event = function() {
				if (exec(event_index) !== false) {
					bind_next_event(event_index);
				}
			};
			if (typeof phy['events'][event_index]['exectimeout'] !== 'undefined') {
				setTimeout(my_exec_event, phy['events'][event_index]['exectimeout']);
			} else {
				my_exec_event();
			}
		};
	};
	var bind = function(event_index) {
		var event = phy['events'][event_index]['event'];
		var elements = null;
		if (typeof event['selector'] !== 'undefined') {
			elements = document.querySelectorAll(event['selector']);
		} else {
			elements = [
				document.getElementById(event['id'])
			];
		}

		for (var i = 0, c = elements.length; i < c; ++i) {
			elements[i].addEventListener(event['type'], gen_exec_event(event_index));
		}
	};
	var bind_next_event = function(current_event_index) {
		for (var i = 1, c = phy['events'].length; i < c; ++i) {  // i = 1 because the first one is onload event
			// If current_event_index is in 'requires' value, bind the event
			for (var j = 0, d = phy['events'][i]['requires'].length; j < d; ++j) {
				if (phy['events'][i]['requires'][j] == current_event_index) {
					if (typeof phy['events'][i]['event'] !== 'undefined') {
						bind(i);
					} else {
						gen_exec_event(i)(event);
					}
				}
			}
		}
	};

	if (
		typeof phy !== 'object'
		|| phy.constructor.name != 'Object'
		|| typeof phy['events'] !== 'object'
		|| phy['events'].constructor.name != 'Array'
		|| phy['events'].length < 1
	) {
		return;
	}

	gen_exec_event(0)();
})(document, window);
